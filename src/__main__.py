import sys
import numpy as np


def main(argv):
    a = np.array([1, 2, 3, 4])
    print(a)


if __name__ == "__main__":
    main(sys.argv[1:])
