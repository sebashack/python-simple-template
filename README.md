## Set work environment

1. Run `make set-py-venv-with-deps`.
2. Run `source .env/bin/activate`.
3. To deactivate the environment run `deactivate`.

## Check style

```
make check-style
```

## Reformat code style

```
make style
```
